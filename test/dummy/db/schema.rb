# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_14_000003) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "hey_nice_work_assertions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "issuer_id", null: false
    t.jsonb "attribs", default: {}, null: false, comment: "Optional BadgeClass Attributes"
    t.datetime "issued_on", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "expires"
    t.boolean "revoked", default: false, null: false
    t.text "revocation_reason"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["issuer_id"], name: "index_hey_nice_work_assertions_on_issuer_id"
  end

  create_table "hey_nice_work_badge_classes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "issuer_id", null: false
    t.string "name", null: false
    t.jsonb "attribs", default: {}, null: false, comment: "Optional BadgeClass Attributes"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["issuer_id"], name: "index_hey_nice_work_badge_classes_on_issuer_id"
  end

  create_table "hey_nice_work_key_pairs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "issuer_id", null: false
    t.string "description"
    t.string "public_key", null: false
    t.string "encrypted_private_key", null: false
    t.string "encrypted_private_key_iv", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["issuer_id"], name: "index_hey_nice_work_key_pairs_on_issuer_id"
  end

  create_table "hey_nice_work_profiles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "active_key_id"
    t.string "name", null: false
    t.jsonb "attribs", default: {}, null: false, comment: "Optional Profile Attributes"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["active_key_id"], name: "index_hey_nice_work_profiles_on_active_key_id"
  end

  add_foreign_key "hey_nice_work_assertions", "hey_nice_work_profiles", column: "issuer_id", on_delete: :cascade
  add_foreign_key "hey_nice_work_badge_classes", "hey_nice_work_profiles", column: "issuer_id", on_delete: :cascade
  add_foreign_key "hey_nice_work_key_pairs", "hey_nice_work_profiles", column: "issuer_id", on_delete: :cascade
  add_foreign_key "hey_nice_work_profiles", "hey_nice_work_key_pairs", column: "active_key_id", on_delete: :nullify
end
