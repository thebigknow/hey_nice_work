module HeyNiceWork
  Config = Hashie::Mash[{
    use_active_storage: true,
    user_model: ::User,
    enc_key:    '55e127b52dee32bc2989080e5e61360af9a24e7bbaa9ec149c2a9cff97170099'
  }]
end
