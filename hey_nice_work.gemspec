$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "hey_nice_work/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "hey_nice_work"
  spec.version     = HeyNiceWork::VERSION
  spec.authors     = ["Dan Wanek"]
  spec.email       = ["dan.wanek@thebigknow.com"]
  spec.summary     = "Hey! Nice work! is an OpenBadges implementaion."
  spec.description = "Hey! Nice work! is an OpenBadges implementaion."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.3"
  spec.add_dependency "active_model_serializers", '~> 0.10.0'
  spec.add_dependency 'attr_encrypted', '~> 3.1.0'
  spec.add_dependency 'hashie', '~> 3.6.0'

  spec.add_development_dependency "pg"
end
