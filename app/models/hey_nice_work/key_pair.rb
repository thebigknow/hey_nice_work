module HeyNiceWork
  class KeyPair < ApplicationRecord
    belongs_to :issuer, class_name: 'Profile'
    attribute :id, :uuid

    attribute :public_key, :string
    attr_encrypted :private_key, key: [HeyNiceWork::Config.enc_key].pack("H*")
  end
end
