module HeyNiceWork
  class Assertion < ApplicationRecord
    attribute :id, :uuid
    attribute :issued_on, :time

    belongs_to :badge, class: BadgeClass
    has_many :recipients, class: HeyNiceWork::Config.user_model

    if HeyNiceWork::Config.use_active_storage
      has_one_attached :image
    end
  end
end
