module HeyNiceWork
  class Profile < ApplicationRecord
    # attribute :id, :uuid

    # NOTE: this can probably just be in the serializer
    # attribute :type, :string, default: 'Issuer'
    attribute :name, :string
    attribute :url, :string
    attribute :description, :string
    attribute :email, :string

    has_many :keys, class_name: 'KeyPair'
    belongs_to :active_key, class_name: 'KeyPair'

    if HeyNiceWork::Config.use_active_storage
      has_one_attached :image
    end

    def generate_key_pair!
    end
  end
end
