module HeyNiceWork
  class BadgeClass < ApplicationRecord
    #attribute :id, :uuid

    # NOTE: this can probably just be in the serializer
    #attribute :type, :string, default: 'BadgeClass'

    attribute :name, :string

    belongs_to :issuer, class_name: 'Profile'

    if HeyNiceWork::Config.use_active_storage
      has_one_attached :image
    end
  end
end
