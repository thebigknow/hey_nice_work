# Hey! Nice Work!

This is an implementation of the [OpenBadges](https://www.imsglobal.org/sites/default/files/Badges/OBv2p0Final/index.html) framework brought to you by the team at [The Big Know](https://www.thebigknow.com)

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'hey_nice_work'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install hey_nice_work
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
