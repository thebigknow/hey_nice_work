class CreateHeyNiceWorkProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :hey_nice_work_profiles, id: :uuid

    create_table :hey_nice_work_key_pairs, id: :uuid do |t|
      t.references :issuer, null: false, type: :uuid, foreign_key: {to_table: :hey_nice_work_profiles, on_delete: :cascade}
      t.string :description, null: true
      t.string :public_key, null: false
      t.string :encrypted_private_key, null: false
      t.string :encrypted_private_key_iv, null: false

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    change_table :hey_nice_work_profiles, id: :uuid do |t|
      t.references :active_key, null: true, type: :uuid, foreign_key: {to_table: :hey_nice_work_key_pairs, on_delete: :nullify}
      t.string :name, null: false
      t.jsonb "attribs", default: {}, null: false, comment: "Optional Profile Attributes"

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
