class CreateHeyNiceWorkBadgeClasses < ActiveRecord::Migration[5.2]
  def change
    create_table :hey_nice_work_badge_classes, id: :uuid do |t|
      t.references :issuer, null: false, type: :uuid, foreign_key: {to_table: :hey_nice_work_profiles, on_delete: :cascade}
      t.string :name, null: false
      t.jsonb "attribs", default: {}, null: false, comment: "Optional BadgeClass Attributes"

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
