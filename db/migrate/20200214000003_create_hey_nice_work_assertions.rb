class CreateHeyNiceWorkAssertions < ActiveRecord::Migration[5.2]
  def change
    create_table :hey_nice_work_assertions, id: :uuid do |t|
      t.references :issuer, null: false, type: :uuid, foreign_key: {to_table: :hey_nice_work_profiles, on_delete: :cascade}
      t.jsonb "attribs", default: {}, null: false, comment: "Optional BadgeClass Attributes"
      t.timestamp :issued_on, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.timestamp :expires, null: true
      t.boolean :revoked, null: false, default: false
      t.text :revocation_reason, null: true

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
